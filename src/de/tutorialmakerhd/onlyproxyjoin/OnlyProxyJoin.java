package de.tutorialmakerhd.onlyproxyjoin;

import org.bukkit.plugin.java.JavaPlugin;

import de.tutorialmakerhd.onlyproxyjoin.listener.PlayerListener;
import de.tutorialmakerhd.onlyproxyjoin.util.MetricsUtil;

public class OnlyProxyJoin extends JavaPlugin {

	public MetricsUtil metricsUtil;
	public PlayerListener playerListener;

	public void onEnable() {
		registerConfig();
		registerUtil();
		registerListener();
	}

	public void onDisable() {
	}

	private void registerConfig() {
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
	}

	private void registerUtil() {
		this.metricsUtil = new MetricsUtil(this);
	}

	private void registerListener() {
		this.playerListener = new PlayerListener(this);
	}
}
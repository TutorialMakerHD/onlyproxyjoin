package de.tutorialmakerhd.onlyproxyjoin.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import de.tutorialmakerhd.onlyproxyjoin.OnlyProxyJoin;

public class PlayerListener implements Listener {

	private final OnlyProxyJoin plugin;

	public PlayerListener(OnlyProxyJoin plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (!event.getAddress().getHostAddress().equals(plugin.getConfig().getString("settings.proxyIP")))
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, plugin.getConfig().getString("settings.playerKickMessage").replaceAll("&", "�"));
	}
}